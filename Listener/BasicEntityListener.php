<?php

namespace Rmoi\CoreBundle\Listener;

use Rmoi\CoreBundle\Listener\BasicListenerModel;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Rmoi\CoreBundle\Model\BasicEntity;



class BasicEntityListener extends BasicListenerModel
{
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var \Rmoi\CoreBundle\Model\BasicEntity $entity */
        $entity = $args->getEntity();

        // Automaticly setting the created at date
        if ($entity instanceof BasicEntity)
        {
            $entity->setCreatedAt(new \DateTime());
        }
    }
}