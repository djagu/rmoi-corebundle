<?php

namespace Rmoi\CoreBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Rmoi\CoreBundle\Listener\BasicEntityListener;


class BasicListenerModel
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
}