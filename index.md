# Rmoi CoreBundle
Adding bases and utilities to Symfony to gain some time.

## Installation

```javascript
        comsposer.json

        "required": {
            "rmoi/core-bundle"
        },
        "repositories": [
            {
                "type": "git",
                "url": "https://djagu@bitbucket.org/djagu/rmoi-corebundle.git"
            }
        ]
```

## Models for entities

### BasicEntity.php (id, createdAt)

The createdAt field is set autmaticlly in a doctrine listener.

### BasicEntityWithName.php (id, createdAt(datetime), name(varchar))

Same as BasicEntity + string name.



## Form type fields

### RmoiTextType

General :
    1. Installer les assets php bin/console asset:install --symlink
    2. Ajouter la personnalisation des formulaires
     ```
        twig:
            form_themes:
                - "@RmoiCoreBundle/Resources/Form/_forms.html.twig"
     ```
     3. Ajouter les fichiers javascript :
     ```
            <script src="{{ asset('bundles/rmoicore/js/jquery.maskedinput.min.js') }}"></script>
            <script src="{{ asset('bundles/rmoicore/js/rmoi-form.js') }}"></script>
     ```
     4. Profier dans le formulaire
       ```
            Ex:
            $builder->->add('sum', RmoiMoneyType::class, array(
                                      'required'      => false,
                                      'addon_html'    => '&euro;',
                                      'regex'         => '^\d+$',
                                  ))
       ```


Options :
    'regex'             => "^[0-2]$", // Verifie si le champ correspond bien a la regex JS + PHP
    'addon_html'        => "html ...", // Ajoute un input-grou-addon automatiquement
    'mask'              => "", // Formatte l'input d'une certaine maniere en JS (cd. MASK OPTION)
    'mask_placeholder'  => "" // Also set the normal placeholder attribut
            
MASK options:
     *      a - Represents an alpha character (A-Z,a-z)
     *      9 - Represents a numeric character (0-9)
     *      * - Represents an alphanumeric character (A-Z,a-z,0-9)
     

### RmoiNumberType (extends RmoiTextType)

General:
    Ajouter ce plugin en plus :
    ``` 
        <script src="{{ asset('bundles/rmoicore/js/jquery.number.min.js') }}"></script>
    ```       

Options supplementaires:
    'decimals'              => 2,
    'decimal_point'         => '.',
    'thousands_separator'   => '.'





