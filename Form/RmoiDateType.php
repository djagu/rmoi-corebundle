<?php

namespace Rmoi\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RmoiDateType extends RmoiTextType
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'regex'                 => null, // Le mask ce charge de la regex
            'widget'                => 'single_text',
            'html5'                 => false,
            'format'                => 'dd/MM/yyyy',
            'mask'                  => '99/99/9999',
            'mask_placeholder'      => 'JJ/MM/AAAA',
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
    }

    public function getBlockPrefix()
    {
        return "rmoi_date";
    }

    public function getParent()
    {
        return DateType::class;
    }
}