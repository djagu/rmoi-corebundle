<?php

namespace Rmoi\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RmoiNumberType extends RmoiTextType
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->addModelTransformer(new CallbackTransformer(function($number){
            return $number;
        }, function($text){
            return floatval(str_replace(',', '.', str_replace(" ", "", $text)));
        }));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefaults(array(
            'regex'                 => null,
            'mask'                  => null,
            'decimals'              => 2,
            'decimal_point'         => '.',
            'thousands_separator'   => ' '
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars['rm_number_decimals'] = $options['decimals'];
        $view->vars['rm_number_decimal_point'] = $options['decimal_point'];
        $view->vars['rm_number_thousands_separator'] = $options['thousands_separator'];
    }

    public function getBlockPrefix()
    {
        return "rmoi_number";
    }
}