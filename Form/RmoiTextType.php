<?php

namespace Rmoi\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RmoiTextType
 * @package Rmoi\CoreBundle\Form
 *
 *
 * Working with the jquery.maskedinput.js plugin
 */
class RmoiTextType extends AbstractType
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Handling the regex validation
        $builder->addModelTransformer(new CallbackTransformer(function($model){
            return $model;
        }, function($model){
            if (!empty($options['regex']))
            {
                if (preg_match_all($options['regex'], $model) === false)
                {
                    throw new TransformationFailedException();
                }
            }
            return $model;
        }));
    }

    /**
     * @param OptionsResolver $resolver
     *
     *
     *     MASK options:
     *      a - Represents an alpha character (A-Z,a-z)
     *      9 - Represents a numeric character (0-9)
     *      * - Represents an alphanumeric character (A-Z,a-z,0-9)
     *
     *      EX: 99/99/9999
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'regex'             => "",
            'addon_html'        => NULL,
            'mask'              => "",
            'mask_placeholder'  => "",
            'has_feedback_icon' => NULL,
        ));
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['addon_html'] = $options['addon_html'];
        $view->vars['regex'] = $options['regex'];
        $view->vars['mask'] = $options['mask'];
        $view->vars['mask_placeholder'] = $options['mask_placeholder'];
        $view->vars['has_feedback_icon'] = $options['has_feedback_icon'];
    }

    public function getBlockPrefix()
    {
        return "rmoi_text";
    }

    public function getParent()
    {
        return TextType::class;
    }
}