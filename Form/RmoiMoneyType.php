<?php

namespace Rmoi\CoreBundle\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RmoiMoneyType extends RmoiTextType
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
    }

    public function getBlockPrefix()
    {
        return "rmoi_money";
    }

    public function getParent()
    {
        return MoneyType::class;
    }
}