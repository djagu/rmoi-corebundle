/************************************************
 * Handling the generic form types addons
 ************************************************/

function initRmoiJs()
{

    /************************************************
     * RmoiTextType
     ************************************************/

    $('.rmoi-text-input').each(function(){

        // Gestion du mask
        var mask = $(this).attr('data-rm-mask');
        var mask_placeholder = $(this).attr('data-rm-mask-placeholder');
        mask_placeholder = (mask_placeholder == undefined) ? "" : mask_placeholder;

        if (mask != undefined && mask.length > 0)
        {
            $(this).mask(mask, { placeholder: mask_placeholder });
        }

        // Gestion de la verification regex
        var rmRegex = $(this).attr('data-rm-regex');
        var regex = new RegExp(rmRegex);

        if (rmRegex !== undefined && rmRegex.length > 0)
        {
            $(this).on('keypress', function (event) {
                var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                var currentInput = $(this).val();
                currentInput = currentInput + key;

                if (!regex.test(currentInput))
                {
                    event.preventDefault();
                    return false;
                }
            });
        }
    });


    /************************************************
     * RmoiNumberType
     ************************************************/
    $('.rmoi-number-input').each(function(){

        var decimals = $(this).attr('data-rm-number-decimals');
        if (decimals === undefined)
            decimals = 0;
        decimals = parseInt(decimals);

        var decimal_point = $(this).attr('data-rm-number-decimal-point');
        if (decimal_point === undefined)
            decimal_point = '';
        var thousands_sep = $(this).attr('data-rm-number-thousands-separator');
        if (thousands_sep === undefined)
        {
            thousands_sep = '';
        }


        $(this).number(true, decimals, decimal_point, thousands_sep);

    });



    /************************************************
     * RmoiMoneyType
     ************************************************/

}

jQuery(document).ready(function($){

    initRmoiJs();

});