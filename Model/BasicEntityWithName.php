<?php

namespace Rmoi\CoreBundle\Model;

use Rmoi\CoreBundle\Model\BasicEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperClass
 */
class BasicEntityWithName extends BasicEntity
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}